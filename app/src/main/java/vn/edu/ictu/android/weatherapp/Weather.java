package vn.edu.ictu.android.weatherapp;

public class Weather {
    private String location;
    private int temperature;
    private int icon;

    public String getLocation() {
        return location;
    }

    public Weather setLocation(String location) {
        this.location = location;
        return this;
    }

    public int getTemperature() {
        return temperature;
    }

    public Weather setTemperature(int temperature) {
        this.temperature = temperature;
        return this;
    }

    public int getIcon() {
        return icon;
    }

    public Weather setIcon(int icon) {
        this.icon = icon;
        return this;
    }
}
