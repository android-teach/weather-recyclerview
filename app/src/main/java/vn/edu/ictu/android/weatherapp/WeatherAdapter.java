package vn.edu.ictu.android.weatherapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.ViewHolder> {
    private Context context;
    private List<Weather> weatherList;

    public WeatherAdapter(Context context, List<Weather> weatherList) {
        this.context = context;
        this.weatherList = weatherList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_weather, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int pos) {
        Weather weather = weatherList.get(pos);
        viewHolder.tvLocation.setText(weather.getLocation());
        viewHolder.tvTemperature.setText(weather.getTemperature() + "C");
        viewHolder.ivItem.setImageResource(weather.getIcon());
    }

    @Override
    public int getItemCount() {
        return weatherList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvLocation, tvTemperature;
        ImageView ivItem;

        public ViewHolder(View itemView) {
            super(itemView);

            tvLocation = itemView.findViewById(R.id.tvLocation);
            tvTemperature = itemView.findViewById(R.id.tvTemperature);
            ivItem = itemView.findViewById(R.id.ivItem);
        }
    }
}
