package vn.edu.ictu.android.weatherapp;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<Weather> weatherList = new ArrayList<>();
    private WeatherAdapter adapter;

    private EditText editLocation, editTemperature;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        editLocation = findViewById(R.id.editLocation);
        editTemperature = findViewById(R.id.editTemperature);
        findViewById(R.id.btnAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addWeather();
            }
        });

        RecyclerView rvWeather = findViewById(R.id.rvWeather);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvWeather.setLayoutManager(layoutManager);
        adapter = new WeatherAdapter(this, weatherList);
        rvWeather.setAdapter(adapter);
    }

    private void addWeather() {
        String location = editLocation.getText().toString().trim();
        String temp = editTemperature.getText().toString().trim();
        int temperature = Integer.parseInt(temp);

        Weather w = new Weather();
        w.setLocation(location);
        w.setTemperature(temperature);
        if (temperature < 20) w.setIcon(R.drawable.ic_rain);
        if (temperature >= 20 && temperature <= 30) w.setIcon(R.drawable.ic_cloud);
        if (temperature > 30) w.setIcon(R.drawable.ic_sunny);

        weatherList.add(w);
        adapter.notifyDataSetChanged();

        editLocation.setText("");
        editTemperature.setText("");
        editLocation.requestFocus();
    }
}
